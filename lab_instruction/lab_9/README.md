Lab 9 : Oauth, Webservice, beserta Pengenalan Cookie dan Session


CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018



Learning Objectives

What you should understand after finishing this tutorial :

What Cookie and Session is
Understands how cookie and session works 
Will be able to use Cookie and Session

End Result
Using session for :
Implement Login and Logout
Created and Delete list of favourite item

Using cookies for:
Implement Login and Logout

Create profile and login page (for Cookie and Session method) 

Self-Reflection

Before doing this tutorial, let’s review some basic knowledge ! :)

What is HTTP ? 
What is basic concepts of HTTP ?
How important Cookie & Session are?

Introduction

HTTP

HTTP stands for Hypertext Transfer Protocol which the protocol used between clients and server. HTTP is stateless, which means between states or activities that one with another is independent (not connected). Every new activity, no previous data saved for the new activity.
For example throwing dice activity, the first throw, second, third etc, every each throw doesn’t have correlation. For every each throw, the result is never affected by the previous throw or the next throw.

Noted: HTTP is stateless. 

Basic knowledge of HTTP

Some basic knowledge about HTTP are: 

Client/Server: Interaction occur by client/server. If client reqest then server respond to the request. 
 Stateless: Every activity (request/response) is independent
Application Layer : Website run on application layer. The request/response process happens on the transport layer which usually using TCP Protocol not HTTP, that determine how data is sent. Application layer doesn’t care how the data is sent or processed on Transport Layer. App layer only focus on request and response.
Client Actions Method: Method used by the client when request something to the server. E.g : GET, POST, PUT, DELETE, etc. 
Server status code:Code status given by the server when requesting a web page. E.g: 200 (OK), 404 (Page not found), 500 (Internal Server Error), etc. 
Headers: Header is little information that sent with the request and response. That informations used as additional data for request and response. E.g : Headers contain content-type:json. Which means content-type that being requested is json. Headers are also contain cookies data.


Cookies & Session Backgrounds

Every communication between client and server are done within HTTP Protocol, where HTTP is a stateless protocol. Which means, one’s state and the others are independent. It requires a client computer that can run browser to create a TCP connection to server everytime it do a request. Without a persistent connection between client and server, softwares on each endpoints can’t depend only on TCP connection to do holding state or holding session state. What does holding state means?

For example, you want to access page A on a web that requires the user logged in into the web. You log in into the web and successfully open page A. When you want to change to page B on the same web, without a holding state, you will be asked to re-log. Those thing will always happen when you access different pages even if it’s still on the same website. The process to tell ‘who’ logged on and storing the data is known as a dialog between client-server and is used as a base for session - a semi-permanent exchange of information. It is a hard thing to do to make HTTP do holding state (because HTTP is a stateless protocol). Because of that, we need a technique to handle those problems, which is Cookie and session.
Note: HTTP is stateless. How to make web (https) become stateful?


Cookies & Session

One of the common ways to create holding state is using session ID which is stored as a cookie on client’s computer. Session ID can be recognized as a token (line of char) to determine a unique session on certain web apps. We only store Session ID rather than storing all kinds of information such as username, name and password. This Session ID then can be mapped to a data structure on web server side. On that data structure, you can store all kinds of information you need. This method is safer for storing user’s information, rather than storing it inside the cookie. With this method, the information will not be abused by client or suspicious connection. Besides, this method is more ‘accurate’ if there are lots of data to be stored. Its because cookie can only store a maximum of 4kb data. Imagine you are logged on into a web/application and get session ID (Session Identifier). To create holding state on stateless HTTP, browsers usually sends a session ID to server every request. By that, everytime a request come , then server will identify the user. Then the server will search for state information on server memory or in database based on the session ID, and return the asked data.

Important difference that needs to be remembered, Cookie data are stored on client’s end, while Session data are typically stored on server’s end. Do you still not understand what is stateless, stateful, cookies and session? Try to read this Article: Stateless, Stateful, Coookies and Session

Then about storages (data storage), you also need to understand the difference between Cookies, Session Storage and Local Storage. See the image below, 


Here are some videos that might help you to understand about Cookies and Session: Session_Cookies, Cookies_History, Difference between Cookies-Session-Local-Storage

Note: Cookies and Session make web (HTTP) stateful.

Additional Information


To see Cookies data, use the beloved Chrome browser, 


Press F12 or right click -> Inspect element
Choose APPLICATION tab
Then on the left sidebar, choose Cookies,
Choose your web’s sub-menu (for example localhost:8000 if you work in local)
Then it will show some datas for example csrftoken with values, domain, expiry date, etc

Why storing data (for example logged in user data) on cookies are not safe?
Is storing data with session are more safer? How to prove this?

Django framework have ‘key’ cookies called sessionid to store unique token every time the user successfully login to a web. For example, run a server on localhost:8000, open page A and login. Then open other page (page B) using incognito, open the same web and make sure you haven’t login to this page B. Copy sessionid (key-value) from page A when you have logged on, then fill the cookie manually on page B. Press F5. Analyze what happen!

Note: Cookie are checked from client side, session are checked from server side. If a login session are deleted (logout), then all login using the same session will automatically log-out, because server-side checks. This thing wont work on cookies.\

Creating Application Page: Login, Profile,

Start your virtual environment
Create new apps called lab_9, Register it on INSTALLED_APPS

Create new test Case inside lab_9/tests.py

Commit and Push your work, then you will see your UnitTest have error.

Add configuration to praktikum/urls.py for app lab_9 (if you forget how, check the past lab_instruction)

Create a URL configuration on lab_9/urls.py:

from django.conf.urls import url
from .views import index, profile, \
    add_session_drones, del_session_drones, clear_session_drones, \
    cookie_login, cookie_auth_login, cookie_profile, cookie_clear

# sol to challenge
from .views import add_session_item, del_session_item, clear_session_item
# /sol
from .custom_auth import auth_login, auth_logout

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile/$', profile, name='profile'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),

    #add/delete drones
    url(r'^add_session_drones/(?P<id>\d+)/$', add_session_drones, name='add_session_drones'),
    url(r'^del_session_drones/(?P<id>\d+)/$', del_session_drones, name=''),
    url(r'^clear_session_drones/$', clear_session_drones, name='clear_session_drones'),

    # cookie
    url(r'^cookie/login/$', cookie_login, name='cookie_login'),
    url(r'^cookie/auth_login/$', cookie_auth_login, name='cookie_auth_login'),
    url(r'^cookie/profile/$', cookie_profile, name='cookie_profile'),
    url(r'^cookie/clear/$', cookie_clear, name='cookie_clear'), #sekaligus logout dari cookie

]

Note: use this 3 additional files (csui_helper.py, api_enterkomputer.py, custom_auth.py) which are all one level with views.py) this 3 files has its own functions.

NOTE: USE THE ACCESS GIVEN WITH FULL OF RESPONSIBILITY.  POWER COMES WITH GREAT RESPONSIBILITY. 



Create a file called csui_helper.py :

import requests

API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
def get_access_token(username, password):
    try:
        url = "https://akun.cs.ui.ac.id/oauth/token/"

        payload = "username=" + username + "&password=" + password + "&grant_type=password"
        headers = {
            'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
            'cache-control': "no-cache",
            'content-type': "application/x-www-form-urlencoded"
        }
        response = requests.request("POST", url, data=payload, headers=headers)

        return response.json()["access_token"]
    except Exception as e:
        return None
        # raise Exception("username atau password sso salah, input : [{}, {}]".format(username, password,))

def get_client_id():
    client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
    return client_id

def verify_user(access_token):
    print ("#get identity number")
    parameters = {"access_token": access_token, "client_id": get_client_id()}
    response = requests.get(API_VERIFY_USER, params=parameters)
    print ("response => ", response.json())
    return response.json()

def get_data_user(access_token, id):
    print ("#get data user => ", id)
    parameters = {"access_token": access_token, "client_id": get_client_id()}
    response = requests.get(API_MAHASISWA+id, params=parameters)
    print ("response => ", response.text)
    print ("response => ", response.json())
    return response.json()

Create a file api_enterkomputer.py :

import requests

DRONE_API       = 'https://www.enterkomputer.com/api/product/drone.json'
SOUNDCARD_API   = 'https://www.enterkomputer.com/api/product/soundcard.json'
OPTICAL_API     = 'https://www.enterkomputer.com/api/product/optical.json'

def get_drones():
    drones = requests.get(DRONE_API)
    return drones

# Compete the SOUNDCARD_API and OPTICAL_API call to do CHALLENGE


Create a file custom_auth.py :

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from .csui_helper import get_access_token, verify_user

#authentication
def auth_login(request):
    print ("#==> auth_login ")

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        #call csui_helper
        access_token = get_access_token(username, password)
        if access_token is not None:
            ver_user = verify_user(access_token)
            kode_identitas = ver_user['identity_number']
            role = ver_user['role']

            # set session
            request.session['user_login'] = username
            request.session['access_token'] = access_token
            request.session['kode_identitas'] = kode_identitas
            request.session['role'] = role
            messages.success(request, "Anda berhasil login")
        else:
            messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('lab-9:index'))

def auth_logout(request):
    print ("#==> auth logout")
    request.session.flush() # menghapus semua session

    messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
    return HttpResponseRedirect(reverse('lab-9:index'))

Insert this code into views.py

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
#catatan: tidak bisa menampilkan messages jika bukan menggunakan method 'render'
from .api_enterkomputer import get_drones

response = {}

#NOTE : To help you understand the goals of a function (def)
#Please explain the function using your own language, above those functions

# ======================================================================== #
# User Func
# What does this function do? Change with your own words

def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:
        html = 'lab_9/session/login.html'
        return render(request, html, response)

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['drones'] = get_drones().json()

    # print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
    ## handling so there will not be an error when the first time login (empty session)
    if 'drones' in request.session.keys():
        response['fav_drones'] = request.session['drones']
    # If there are no else, cache will still save the data
    # on response, therefore it will not be up-to-date
    else:
        response['fav_drones'] = []

def profile(request):
    print ("#==> profile")
    ## sol : how to prevent error, if url profile are accessed directly
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))
    ## end of sol

    set_data_for_session(response, request)

    html = 'lab_9/session/profile.html'
    return render(request, html, response)

# ======================================================================== #

### Drones
def add_session_drones(request, id):
    ssn_key = request.session.keys()
    if not 'drones' in ssn_key:
        print ("# init drones ")
        request.session['drones'] = [id]
    else:
        drones = request.session['drones']
        print ("# existing drones => ", drones)
        if id not in drones:
            print ('# add new item, then save to session')
            drones.append(id)
            request.session['drones'] = drones

    messages.success(request, "Berhasil tambah drone favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_drones(request, id):
    print ("# DEL drones")
    drones = request.session['drones']
    print ("before = ", drones)
    drones.remove(id) #untuk remove id tertentu dari list
    request.session['drones'] = drones
    print ("after = ", drones)

    messages.error(request, "Berhasil hapus dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_drones(request):
    print ("# CLEAR session drones")
    print ("before 1 = ", request.session['drones'])
    del request.session['drones']

    messages.error(request, "Berhasil reset favorite drones")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# ======================================================================== #
# COOKIES

# What is this function do? #You can replace that function but you should give your explanation
def cookie_login(request):
    print ("#==> masuk login")
    if is_login(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)

def cookie_auth_login(request):
    print ("# Auth login")
    if request.method == "POST":
        user_login = request.POST['username']
        user_password = request.POST['password']

        if my_cookie_auth(user_login, user_password):
            print ("#SET cookies")
            res = HttpResponseRedirect(reverse('lab-9:cookie_login'))

            res.set_cookie('user_login', user_login)
            res.set_cookie('user_password', user_password)

            return res
        else:
            msg = "Username atau Password Salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))

def cookie_profile(request):
    print ("# cookie profile ")
    # this method is used to prevent error when URL is accessed directly
    if not is_login(request):
        print ("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        # print ("cookies => ", request.COOKIES)
        in_uname = request.COOKIES['user_login']
        in_pwd= request.COOKIES['user_password']

        # If cookie is set manually, it should be stop this way so it can re-enter
        # so delete set up cookie manually 
        if my_cookie_auth(in_uname, in_pwd):
            html = "lab_9/cookie/profile.html"
            res =  render(request, html, response)
            return res
        else:
            print ("#login dulu")
            msg = "Kamu tidak punya akses :P "
            messages.error(request, msg)
            html = "lab_9/cookie/login.html"
            return render(request, html, response)

def cookie_clear(request):
    res = HttpResponseRedirect('/lab-9/cookie/login')
    res.delete_cookie('lang')
    res.delete_cookie('user_login')

    msg = "Anda berhasil logout. Cookies direset"
    messages.info(request, msg)
    return res

# What is this function do?
def my_cookie_auth(in_uname, in_pwd):
    my_uname = "utest" #You can replace it with the USERNAME you want
    my_pwd = "ptest" #You can replace it with the PASSWORD you want
    return in_uname == my_uname and in_pwd == my_pwd

# What is this function do? 
def is_login(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES

This lab doesn’t use model
Create a file lab_9/templates/lab_9/layout/base.html

{% load staticfiles %}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="LAB 9">
    <meta name="author" content="{{author}}">

    <!-- bootstrap csss -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .rata-tengah {
            text-align: center;
            margin : 20px;
        }
        .judul {
            text-transform:uppercase;
            margin-bottom: 50px;
            margin-top: 50px;
        }
    </style>
    <title>
        {% block title %} Lab 9 By {{author}} {% endblock %}
    </title>
</head>
<body>
<header>
    <h1 style="text-align:center">
        <small><em> Change This With Your Custom Header </em></small>
    </h1>
    <!-- Your Header Here -->
</header>
<content>
    <div class="container">
        {% for message in messages %}
        <div class="alert {{ message.tags }} alert-dismissible" role="alert" id="django-messages">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-right: 15px;">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ message }}
        </div>
        {% endfor %}

        {% block content %}
        <!-- Your Content Here -->
        {% endblock %}
    </div>
</content>
<footer>
    <hr>
    {% block footer %}

    <h1 style="text-align:center">
        <small><em> Change This With Your Custom Footer </em></small>
    </h1>
    <!-- Your Footer Here -->
    {% endblock %}
</footer>

<!-- Jquery n Bootstrap Script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>

You are not given any CSS files or Header and Footer files. You are asked to design the web appearance so you have to implement your best designs. 

See that we only use 2 methods, Session and Cookies. The HTML file folders are separated but it has the same name, therefore it is recommended to be careful on creating a directory structure so you wouldn’t called the wrong method

Implementation Session

Login Session: Create a file lab_9/templates/lab_9/session/login.html to simulate the login using session

{% extends "lab_9/layout/base.html" %}
{% block content %}
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="rata-tengah">
            <div class="judul">
                <h1> Halaman Login </h1>
                <p class="text-info"> Gunakan <b> akun SSO </b> untuk login </p>
            </div>
            <form action="{% url 'lab-9:auth_login' %}" method="POST">
                {% csrf_token %}
                <p>
                    <label for="username"> Your username </label>
                    <input type="text" id="username" name="username" required>
                </p>
                <p>
                    <label for="password"> Your password </label>
                    <input type="password" id="password" name="password" required>
                </p>
                <input type="submit" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
{% endblock %}


Profile Session: Create a file lab_9/templates/lab_9/session/profile.html

{% extends "lab_9/layout/base.html" %}
{% block content %}
<!-- Content Here -->
<div class="pojok-kanan">
</div>
<br>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2> [Session] Profile </h2>
    </div>
    <div class="panel-body">
        <p> Username : {{ author }} </p>
        <p> NPM : {{kode_identitas}} </p>
        <p> Role : {{ role }} </p>
    </div>
    <div class="panel-footer">
        <a href="{% url 'lab-9:auth_logout' %}" class="btn btn-danger pull-right" onclick="return confirm('Keluar?')">
            Logout </a>
        <a href="{% url 'lab-9:cookie_login' %}" class="btn btn-info"> Masuk Halaman Cookies </a>
    </div>
</div>

<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active">
            <a href="#drones" aria-controls="home" role="tab" data-toggle="tab"> Drones </a>
        </li>
        <li role="presentation">
            <a href="#soundcard" aria-controls="settings" role="tab" data-toggle="tab"> Soundcard </a>
        </li>
        <li role="presentation">
            <a href="#" aria-controls="settings" role="tab" data-toggle="tab"> Optical </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="drones">
            {% include 'lab_9/tables/drones.html' %}
        </div>
        <div role="tabpanel" class="tab-pane fade" id="soundcard">
            <!-- Apply the same here -->
        </div>
        <div role="tabpanel" class="tab-pane fade" id="optical">
            <!-- Apply the same here -->
        </div>
    </div>
</div>
{% endblock %}


Additional files: create lab_9/templates/lab_9/tables/drones.html
<div class="panel panel-info">
    <div class="panel-heading">
        <h2> Daftar Drones : {{ drones | length}} </h2>
        <a href="{% url 'lab-9:clear_session_drones' %}" class="btn btn-danger" onclick="return confirm('Reset data?')">
            Reset Favorite Drones
        </a>
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <th> No</th>
            <th> Nama</th>
            <th> Harga</th>
            <th> Jumlah</th>
            <th> Aksi </th>

            </thead>
            <tbody>
            {% for drone in drones %}
            <tr>
                <td> {{ forloop.counter }}</td>
                <td> {{ drone.name }}</td>
                <td> {{ drone.price }}</td>
                <td> {{ drone.quantity }}</td>
                <td>
                    {% if not drone.id in fav_drones %}
                    <a href="{% url 'lab-9:add_session_drones' drone.id %}" class="btn btn-primary"> Favoritkan </a>
                    {% else %}
                    <a href="{%url 'lab-9:del_session_drones' drone.id %}" class="btn btn-primary"> Hapus dari favorit </a>
                    {% endif %}
                </td>
            </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>

Cookie Implementation


Cookie Login: Create file lab_9/templates/lab_9/cookie/login.html

{% extends "lab_9/layout/base.html" %}
{% block content %}
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="rata-tengah">
            <div class="judul">
                <h1> Login using COOKIES </h1>
                <p class="text-danger"> Do not use <b> Real SSO Account</b> </p>
                <p class="text-danger"> Because Username and password will be stored in cookies </p>
            </div>
            <form action="{% url 'lab-9:cookie_auth_login' %}" method="POST">
                {% csrf_token %}
                <p>
                    <label for="username"> Your username* </label>
                    <input type="text" id="username" name="username" required>
                </p>
                <p>
                    <label for="password"> Your password* </label>
                    <input type="password" id="password" name="password" required>
                </p>
                <input type="submit" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
{% endblock %}


Cookie Profile: make lab_9/templates/lab_9/cookie/profile.html:

{% extends "lab_9/layout/base.html" %}

{% block content %}
<br>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2> [Cookie] Profile </h2>
    </div>
    <div class="panel-body">
        <p> Username : {{ request.COOKIES.user_login }} </p>
    </div>
    <div class="panel-footer">
        <a href="{% url 'lab-9:cookie_clear' %}" class="btn btn-danger"> Reset Cookies (Logout) </a>
    </div>
</div>
{% endblock %}




Challenge

To help you doing this challenge, we prepared a view line of code to help


Delete/Insert Item implementation form session generally:

### General Function
def add_session_item(request, key, id):
    print ("#ADD session item")
    ssn_key = request.session.keys()
    if not key in ssn_key:
        request.session[key] = [id]
    else:
        items = request.session[key]
        if id not in items:
            items.append(id)
            request.session[key] = items

    msg = "Berhasil tambah " + key +" favorite"
    messages.success(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_item(request, key, id):
    print ("# DEL session item")
    items = request.session[key]
    print ("before = ", items)
    items.remove(id)
    request.session[key] = items
    print ("after = ", items)

    msg = "Berhasil hapus item " + key + " dari favorite"
    messages.error(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_item(request, key):
    del request.session[key]
    msg = "Berhasil hapus session : favorite " + key
    messages.error(request, msg)
    return HttpResponseRedirect(reverse('lab-9:index'))

# ======================================================================== #


Equipped with urls.py:

#general function : solution to challenge
    url(r'^add_session_item/(?P<key>\w+)/(?P<id>\d+)/$', add_session_item, name='add_session_item'),
    url(r'^del_session_item/(?P<key>\w+)/(?P<id>\d+)/$', del_session_item, name='del_session_item'),
    url(r'^clear_session_item/(?P<key>\w+)/$', clear_session_item, name='clear_session_item'),




Checklist


Mandatory
1.  Session: Login & Logout
    1. [ ] Implement Login function
    2. [ ] Implement Logout function

2.  Session: Favourites
    1. [ ] Implement function “Favoritkan” for Drones
    2. [ ] Implement function “Hapus dari favorit” for Drones
    3. [ ] Implement function “Reset favorit” for Drones

3. Cookies: Login & Logout
    1. [ ] Implement Login function
    2. [ ] Implement Logout function


4. Implement Header and Footer
    1. [ ] Make header funtion with logout button only if user has logged in (either on session or on cookies). Make it as good and interesting as possible.

5. Make sure you have a good _Code Coverage_
    1. [ ]  If you have not done the configuration to show _Code Coverage_ in Gitlab, you must configure it in `Show Code Coverage in Gitlab` on [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md) 
    2. [ ] Make sure your _Code Coverage_ is 100%.



Challenge



Implementasi API Optical dan SoundCard

1. API Optical and SoundCard Implementation 
    1. [ ] Add link to the Optical and SoundCard tab on Session Profile page
    2. [ ] Create table contains of data optical/soundcard

2.  Implement general function already provided to manage session :
    1. [ ] Use general function to add (add favorite) optical/soundcard to session
    2. [ ] Use gerenal funcion to delete (delete from favorite) optical/soundcard from session
    3. [ ] Use gerenal function to delete/reset category (drones/optical/soundcard) from session

3. Implement session for all page made on the previous labs
    1. [ ] If the lab page can be accessed without login first, then the login page will be shown
    2. [ ] If the lab-N page can be accessed without login first, the after login, lab-N page will be shown
    3. [ ] Change csui_helper.py implementation on Lab 9 so it can be used for Lab 7 (You can delete csui_helper.py on the Lab 7)



