# Lab 4: Introduction to _HTML5_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## LAB 4 Objectives

What should you learn from codes & documentation in this sub-directory?

- Understand how _HTML5_ works
- Know various _HTML5_ tags
- Understand the programming logic _for_ and _if_ in _Django_ template



## Checklist

1. I've created my _Home Page_
    1. [ ] I've created a new application `lab-4`
    2. [ ] In my new `app lab-4`, I've already set my file/directory structure consists of:
        ```
            - lab_4
                __init__.py
                admin.py
                apps.py
                models.py
                tests.py
                views.py
                - migrations
                    ...
                - templates
                    - layout
                        base.html
                    - partials
                        header.html
                        footer.html
                    table.html
                    lab_4.html
                    from_result.html
        ```
    3. [ ] I've already fill my `navbar.html` and `footer.html` with _HTML5_ tag. Make sure that `navbar.html` include `<nav>` and `footer.html` include copyright symbol &copy;
    4. [ ] I've already fill my `base.html` with _HTML5_ tag. I've describe myself in my _Home Page_. References:
        https://www.html-5-tutorial.com/all-html-tags.htm
        https://www.w3schools.com/TAGs/
    5. [ ] In my _Home Page_, I've already create a _Form_ to submit a message Pada home page
2. I've create a _Page to show every messages submitted
    1. [ ] I've create an HTML Table to show every messages submitted
    2. [ ] I've assign a different colour for messages posted by Anonymous
3. I've already got good _Code Coverage_
    1. [ ] I've configure my GitLab to show _Code Coverage_ in my GitLab front page. NB: If you haven't done the configuration, please read the Wiki on this URL [https://gitlab.com/PPW-2017/ppw-lab-ki/wikis/show-code-coverage](https://gitlab.com/PPW-2017/ppw-lab-ki/wikis/show-code-coverage)
    2. [ ] I've managed to get 100% _Code Coverage_ for all of my Code in Lab 4
4. Additional
    1. [ ] I've display my Photo. I've use `<img>` tag in my _Home Page_ so that my Photo will displayed in circle form template
    2. [ ] I've edit my _Home Page_ and make it more attractive
    3. [ ] I've create a new Test to check the availability of _Navbar_ and _Copyright_
    4. [ ] I've managed to show error message whenever my `Message` were fill with blank. I've already create my _Test_ before the _Implementation_
    5. [ ] I've edit my _custom_ error message. I've already create my _Test_ before the _Implementation_
    6. [ ] I've change the web redirection scenario. The _Root_ URL (`<YOURAPPNAME>.herokuapp.com`) will automatically redirectod to the _Home Page_ in Lab 4. I've already create my _Test_ before the _Implementation_. 
    7. [ ] I've change my _datetime_ using Local Timezone (GMT + 7)

## Step-by-step for Lab 4

#### Create new _Home Page_

1. Create new _Django Apps_ `lab_4`
1. Install `lab_4` as INSTALLED_APPS in `praktikum/settings.py`
1. Create new _Test_ in `lab_4/tests.py` :
    ```python
        from django.test import TestCase
        from django.test import Client
        from django.urls import resolve
        from django.http import HttpRequest
        from .views import index, about_me, landing_page_content
        # Create your tests here.
        
        class Lab4UnitTest(TestCase):
            def test_lab_4_url_is_exist(self):
                response = Client().get('/lab-4/')
                self.assertEqual(response.status_code, 200)
             
            def test_about_me_more_than_6(self):
               self.assertTrue(len(about_me) >= 6)
            
            def test_lab4_using_index_func(self):
                found = resolve('/lab-4/')
                self.assertEqual(found.func, index)
              
            def test_landing_page_is_completed(self):
                request = HttpRequest()
                response = index(request)
                html_response = response.content.decode('utf8')
        
                #Checking whether have Bio content
                self.assertIn(landing_page_content, html_response)
        
                #Chceking whether all About Me Item is rendered
                for item in about_me:
                    self.assertIn(item,html_response)      
    ```
1. _Commit_ and  _Push_ your repository. You will see _failed_ _Unit Test_, as expected.
    
    >This time, we'll do RED-GREEN-Refactor in GitLab Pipeline. It's TDD discipline. Yeey :)
    
1. Configure your `praktikum/urls.py` to include `lab_4` so we can call the URL `lab-4`
    ```python
        ........
        import lab_4.urls as lab_4
        
        urlpatterns = [
            .....
            url(r'^lab-4/', include(lab_4, namespace='lab-4')),
        ]
    ```
1. Configure your URL in `lab_4/urls.py`
    ```python
        from django.conf.urls import url
        from .views import index
        
        urlpatterns = [
            url(r'^$', index, name='index'),
         ]
    ```
1. Create a function `index` to handle the _Root_ URL of `lab_4`
    ```python
        from django.shortcuts import render
        from lab_2.views import landing_page_content
        
        # Create your views here.
        response = {'author': ""} #TODO Implement yourname
        about_me = []
        def index(request):
            response['content'] = landing_page_content
            html = 'lab_4/lab_4.html'
            #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
            response['about_me'] = about_me
            return render(request, html, response)
    ```
1. Create `base.html` in `lab_4/templates/lab_4/layout` directory (If the folder is not exist yet, please create the `lab_4/templates/lab_4/layout` directory)
    ```html
    {% load staticfiles %}
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="LAB 4">
        <meta name="author" content="{{author}}">
        <!-- bootstrap csss -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">    
    
        <title> 
            {% block title %} Lab 4 By {{author}}{% endblock %}
        </title>
    </head>
    
    <body>
        <header>
            {% include "lab_4/partials/header.html" %}
        </header>
        <content>
            <div class="container">
                {% block content %}
                    <!-- content goes here -->
                {% endblock %}
            </div>
    
        </content>
        <footer>
            <!-- TODO Block Footer dan include footer.html -->
            {% block footer %}
            {% include "lab_4/partials/footer.html" %}
            {% endblock %}
        </footer>
    
        <!-- Jquery n Bootstrap Script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
    </html>
    ```
1. Create `lab_4.html` in `lab_4/templates/lab_4/`
    ```html
        {% extends "lab_4/layout/base.html" %}
        
        {% block content %}
        <h1>Portofolio</h1>
        <hr/>
        <section name="bio">
            <h2>Bio</h2>
            <p>
                {{content}}
            </p>	
        </section>
        <hr/>
        <section name="about_me">
            <h2>List</h2>
            <ul>
                {% for ii in about_me %}
                <li> {{ ii }} </li>
                {% endfor %}
            </ul>
        </section>
        <hr/>
        {% endblock %}
    ```
1. Create `header.html` in `templates/lab_4/partials`
    ```html
       <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{% url 'lab-4:index' %}">Home</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <!-- LINK MENUJU table, dan link menuju home#form -->
                <li><a href="{% url 'lab-4:index' %}#form"> Form </a></li>
                <li><a href="{% url 'lab-4:result_table' %}"> Messages Table </a> </li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
    ```
1. Create `footer.html` in `templates/lab_4/partials`
    ```html
       <p>&copy; {{author}}</p>
    ```
1. _Copy file_ `lab_4.css` to `lab_4/static/css/` (If the folder is not exist yet, please create the `lab_4/static/css/` directory)
1. _Commit_ and _Push_ your repository. You will see _failed_ _Unit Test_, as expected.

#### Create _Form Message_ 

1. Now, we create a _Form_ using Django _Model Form_. But first, create the _Test_ in `lab_4/tests.py`
    ```python
        from lab_1.views import mhs_name
        from .models import Message
        from .forms import Message_Form
        # Create your tests here.
        
        class Lab4UnitTest(TestCase):
           .........
            def test_model_can_create_new_message(self):
                #Creating a new activity
                new_activity = Message.objects.create(name=mhs_name,email='test@gmail.com',message='This is a test')
                
                #Retrieving all available activity
                counting_all_available_message= Message.objects.all().count()
                self.assertEqual(counting_all_available_message,1)
            
            def test_form_message_input_has_placeholder_and_css_classes(self):
                form = Message_Form()
                self.assertIn('class="form-control"', form.as_p())
                self.assertIn('<label for="id_name">Nama:</label>', form.as_p())
                self.assertIn('<label for="id_email">Email:</label>', form.as_p())
                self.assertIn('<label for="id_message">Message:</label>', form.as_p())
            
            def test_form_validation_for_blank_items(self):
                form = Message_Form(data={'name': '', 'email': '', 'message': ''})
                self.assertFalse(form.is_valid())
                self.assertEqual(
                    form.errors['message'],
                    ["This field is required."]
                )  
    ```
1. _Commit_ and _Push_ your repository. You will see _failed_ _Unit Test_, as expected.
1. Create _Models_ for `Message` in `lab_4/models.py`
    ```python
        from django.db import models
        
        class Message(models.Model):
            name = models.CharField(max_length=27)
            email = models.EmailField()
            message = models.TextField()
            created_date = models.DateTimeField(auto_now_add=True)
            
            def __str__(self):
                return self.message
    ```
1. Create `forms.py` file and insert code below
    ```python
    from django import forms

    class Message_Form(forms.Form):
        error_messages = {
            'required': 'Tolong isi input ini',
            'invalid': 'Isi input dengan email',
        }
        attrs = {
            'class': 'form-control'
        }
        
        name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
        email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=attrs))
        message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
    ```
1. Test your code locally. In this case, we expect the test result should be _successfull_.
    
    >But, it still says error. Why?
    >
    >Please recall, what should we do after create/modify a new _Model_? 

1. Now we show the _form_ and create a function to process the submitted form. Please create a _Test_ below:
    ```python
        .................
        from .views import index, message_post
     
        class Lab4UnitTest(TestCase):
            ............
            def test_lab4_post_fail(self):
                response = Client().post('/lab-4/add_message', {'name': 'Anonymous', 'email': 'A', 'message': ''})
                self.assertEqual(response.status_code, 302)
        
            def test_lab4_post_success_and_render_the_result(self):
                anonymous = 'Anonymous'
                message = 'HaiHai'
                response = Client().post('/lab-4/add_message', {'name': '', 'email': '', 'message': message})
                self.assertEqual(response.status_code, 200)
                html_response = response.content.decode('utf8')
                self.assertIn(anonymous,html_response)
                self.assertIn(message,html_response)
    ```
1. _Commit_ and _Push_ your repository. You will see _failed_ _Unit Test_, as expected.
1. Configure the URL _add_message_ in `lab_4/urls.py`
    ``` python
        ..........
        from .views import index, add_message
        
        urlpatterns = [
            ...........
            url(r'^add_message', message_post, name='add_message'),
        ]
    ```
1. Implement your View file `lab_4/views.py`
    ```python
    .............
    from django.http import HttpResponseRedirect
    from .forms import Message_Form
    from .models import Message
    ...............
 
    def index(request):
        ...............
        response['message_form'] = Message_Form
        return render(request, html, response)
    
    def message_post(request):
        form = Message_Form(request.POST or None)
        if(request.method == 'POST' and form.is_valid()):
            response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
            response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
            response['message'] = request.POST['message']
            message = Message(name=response['name'], email=response['email'],
                              message=response['message'])
            message.save()
            html ='lab_4/form_result.html'
            return render(request, html, response)
        else:        
            return HttpResponseRedirect('/lab-4/')

    ```
1. Modify `lab_4.html` in `lab_4/templates/lab_4/`
    ```html
        ..............
        <section name="form">
            <h2>Send Message</h2>
            <form id="form" method="POST" action="{% url 'lab-4:add_message' %}">
                {% csrf_token %}
                {{ message_form }}
                <br>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </section>
 
        {% endblock %}
    ```
1. Create a file `form_result.html` in `lab_4/templates/lab_4/`
    ```html

        {% extends "lab_4/layout/base.html" %}
        {% block title %} Pesan dari {{ name }} {% endblock %}
        {% block content %}
        <textarea readonly rows="5" class="form-control">&quot;{{message}}&quot;-{{name}} 
            by {{ email }}
        </textarea>
        {% endblock %}
        
        {% block footer %}
            <p> &copy; {{name}} </p>
        {% endblock %}
 
    ```
1. _Commit_ and _Push_ your repository. You should pass the _Unit Test_.

#### View every _Messages_ 

1. We will create a _page_ to show all _Messages_ posted. First, add this _UnitTest_ below:
    ```python
        .......
        from .views import index, message_table, about_me, landing_page_content
        
        
        class Lab4UnitTest(TestCase):
            def test_lab_4_table_url_exist(self):
                response = Client().get('/lab-4/result_table')
                self.assertEqual(response.status_code, 200)
            
            def test_lab_4_table_using_message_table_func(self):
                found = resolve('/lab-4/result_table')
                self.assertEqual(found.func, message_table)
            
            def test_lab_4_showing_all_messages(self):
            
                name_budi = 'Budi'
                email_budi = 'budi@ui.ac.id'
                message_budi = 'Lanjutkan Kawan'
                data_budi = {'name': name_budi, 'email': email_budi, 'message': message_budi}
                post_data_budi = Client().post('/lab-4/add_message', data_budi)
                self.assertEqual(post_data_budi.status_code, 200)
            
                message_anonymous = 'Masih Jelek Nih'
                data_anonymous = {'name': '', 'email': '', 'message': message_anonymous}
                post_data_anonymous = Client().post('/lab-4/add_message', data_anonymous)
                self.assertEqual(post_data_anonymous.status_code, 200)
            
                response = Client().get('/lab-4/result_table')
                html_response = response.content.decode('utf8')
                
                for key,data in data_budi.items():
                    self.assertIn(data,html_response)
                
                self.assertIn('Anonymous', html_response)
                self.assertIn(message_anonymous, html_response
    ```
1. _Commit_ and _Push_ your repository. You will see _failed_ _Unit Test_, as expected.
1. Configure your _result_table_ URL in `lab_4/urls.py`
    ```python
        .........
        from .views import index, message_post, message_table
        
        urlpatterns = [
            .......
            url(r'^result_table', message_table, name='result_table'),
        ]
    ```
1. Add function in `lab_4/views.py`
    ```python
        ............
        def message_table(request):
            message = Message.objects.all()
            response['message'] = message
            html = 'lab_4/table.html'
            return render(request, html , response)
    ```
1. Modify the template file `table.html` in `lab_4/templates/lab_4/` folder
    ```html
        {% extends "lab_4/layout/base.html" %}
        
        {% block title %} Kumpulan Pesan {% endblock %}
        
        {% block content %}
        <!--  Populate semua isi pesan dari model -->
        <table class="table">
            <thead>
                <th>Nama</th>
                <th>Email</th>
                <th>Pesan</th>
                <th>Dibuat Pada</th>
            </thead>
            <tbody>
                <!-- TODO, Kalau nama/email anon maka tr classnya jadi warning -->
                {% for i in message %}
                {% if i.name == 'Anonymous' or i.email == 'Anonymous' %}
                <tr class="warning"> {% else %} <tr class="success">
                    {% endif %}
                    <td> {{i.name}} </td>
                    <td> {{i.email}} </td>
                    <td> {{i.message}} </td>
                    <td> {{i.created_date.time}} </td>
                </tr>             
                {% endfor %}
            </tbody>
        </table>
        
        {% endblock %}
        
    ```
1. _Commit_ and _Push_ your repository. You should pass the _Unit Test_.




## _Django_ Templating

Please visit https://www.html-5-tutorial.com/all-html-tags.htm

#### Django Template Language (DTL)

Django Template Language were designed to simplify the work with HTML. The template itself is simply a text file. The template contains variables and control tags, whose values can be changed when evaluated. Here is an example from DTL:

```html
    {% extends "base_generic.html" %}
    {% block title %}{{ section.title }}{% endblock %}
    
    {% block content %}
    <h1>{{ section.title }}</h1>
    
    {% for story in story_list %}
    <h2>
      <a href="{{ story.get_absolute_url }}">
        {{ story.headline|upper }}
      </a>
    </h2>
    <p>{{ story.tease|truncatewords:"100" }}</p>
    {% endfor %}
    {% endblock %}
```
    
#### Displaying Variables

The variable looks like this: ** {{variable}} **. When a template finds a variable, that variable will be evaluated and replaced with the result. The variable name consists of a combination of alphanumeric and underline characters ("_"). Use dots (".") To access the attributes of a variable.

#### Tags


The Tag syntaks looks like this: ** {% tag%} **. Tags are more complex than variables, some tags produce a text output, some control loops or logic, and load external information into the template.

Some tags require start and end tags. DTL has many *built-in* tags in the template. You can read more on [https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#ref-templates-builtins-tags] (https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#ref-templates-builtins-tags).

**If, Elif, Else**

if, elif and else are used to evaluate a variable and output or run a command. Here is an example of using if, elif and else.
```html
    {% if athlete_list %}
        Number of athletes: {{ athlete_list|length }}
    {% elif athlete_in_locker_room_list %}
        Athletes should be out of the locker room soon!
    {% else %}
        No athletes.
    {% endif %}
```

**For**

```html
    <ul>
    {% for athlete in athlete_list %}
        <li>{{ athlete.name }}</li>
    {% endfor %}
    </ul>
```



